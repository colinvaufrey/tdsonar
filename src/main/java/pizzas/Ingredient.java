package pizzas;

public class Ingredient {

    private final String nom;
    private final boolean vegetarien;

    public Ingredient(String nom, boolean vegetarien) {
        this.nom = nom;
        this.vegetarien = vegetarien;
    }

    public String getNom() {
        return nom;
    }

    public boolean isVegetarien() {
        return vegetarien;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Ingredient) {
            Ingredient i = (Ingredient) o;
            return i.getNom().equals(this.getNom());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return nom.hashCode();
    }

}
